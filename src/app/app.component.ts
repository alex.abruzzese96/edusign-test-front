import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'edusign-test-front';

  constructor(private httpClient: HttpClient, private titleService: Title) {
    titleService.setTitle("Edusign Test || Uploads");
  }

  uploadMultiple(event: any) {
    const files: FileList = event.target.files;
    const formdata = new FormData();

    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      formdata.append('files', element);
    }

    this.httpClient
      .post('http://localhost:3000/uploads', formdata)
      .subscribe(
        (result) => {
          if (files.length > 1){
            window.alert(`Your ${files.length} files has been sended to the server (Check dist folder).`);
          }
          else {
            window.alert('Your file has been sended to the server (check dist folder).');
          }
          console.log(result);
        },
        (error) => {
          window.alert("Error when adding files to server.");
          console.error(error);
        }
      );
  }
}